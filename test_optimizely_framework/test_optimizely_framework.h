//
//  test_optimizely_framework.h
//  test_optimizely_framework
//
//  Created by Michael Gunawan on 10/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for test_optimizely_framework.
FOUNDATION_EXPORT double test_optimizely_frameworkVersionNumber;

//! Project version string for test_optimizely_framework.
FOUNDATION_EXPORT const unsigned char test_optimizely_frameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <test_optimizely_framework/PublicHeader.h>


