//
//  Result.m
//  test_optimizely_framework
//
//  Created by Michael Gunawan on 11/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import "Result.h"

@implementation Result
- (instancetype)init {
    self = [super init];
    return [self enabled:nil service_kitabisa:nil service_promo_claim:nil service_send_money_bank:nil service_send_money_chat:nil service_splitbill:nil];
}

- (nullable instancetype)enabled:(BOOL)enabled
                service_kitabisa:(NSString * _Nullable)service_kitabisa
             service_promo_claim:(NSString * _Nullable)service_promo_claim
         service_send_money_bank:(NSString * _Nullable)service_send_money_bank
         service_send_money_chat:(NSString * _Nullable)service_send_money_chat
               service_splitbill:(NSString * _Nullable)service_splitbill; {

    if (self != nil) {
        self.enabled = enabled;
        self.service_kitabisa = service_kitabisa;
        self.service_promo_claim = service_promo_claim;
        self.service_send_money_bank = service_send_money_bank;
        self.service_send_money_chat = service_send_money_chat;
        self.service_splitbill = service_splitbill;
    }

    return self;
}

@end
