//
//  Result.h
//  test_optimizely_framework
//
//  Created by Michael Gunawan on 11/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Result : NSObject

@property (assign, nonatomic) BOOL enabled;
@property (copy, nonatomic) NSString * _Nullable service_kitabisa;
@property (copy, nonatomic) NSString * _Nullable service_promo_claim;
@property (copy, nonatomic) NSString * _Nullable service_send_money_bank;
@property (copy, nonatomic) NSString * _Nullable service_send_money_chat;
@property (copy, nonatomic) NSString * _Nullable service_splitbill;


- (nullable instancetype)enabled:(BOOL)enabled
                service_kitabisa:(NSString * _Nullable)service_kitabisa
             service_promo_claim:(NSString * _Nullable)service_promo_claim
         service_send_money_bank:(NSString * _Nullable)service_send_money_bank
         service_send_money_chat:(NSString * _Nullable)service_send_money_chat
               service_splitbill:(NSString * _Nullable)service_splitbill;

@end

NS_ASSUME_NONNULL_END
