//
//  Config.h
//  test_optimizely_framework
//
//  Created by Michael Gunawan on 11/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Config : NSObject
@property (copy, nonatomic) NSArray *settings;

-(nullable instancetype)settings:(NSArray*)settings;
@end

NS_ASSUME_NONNULL_END
