//
//  Config.m
//  test_optimizely_framework
//
//  Created by Michael Gunawan on 11/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import "Config.h"

@implementation Config

- (instancetype)init {
    self = [super init];
    return [self settings:@[]];
}
- (nullable instancetype)settings:(NSArray*)settings; {

    if (self != nil) {
        self.settings = settings;
    }

    return self;
}
@end
