//
//  TestOptimizelyService.m
//  test_optimizely_framework
//
//  Created by Michael Gunawan on 10/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import "TestOptimizelyService.h"
#import <OptimizelySDKiOS/OptimizelySDKiOS.h>
#import "Result.h"
#import "Config.h"

@interface TestOptimizelyService ()
@property (strong, nonatomic) OPTLYDatafileManagerDefault *datafileManager;
@property (strong, nonatomic) OPTLYManager *manager;
@property (strong, nonatomic) OPTLYClient *client;
@property (assign, nonatomic) NSInteger *activateDecisionNotificationId;
@property (assign, nonatomic) NSInteger *activateTrackNotificationId;
@end

@implementation TestOptimizelyService
///TODO import pods optimizely to framework project, belajar cara config podfile kalau proj banyak   //donee

- (instancetype)init {
    self = [super init];
    [self defineDataFileManager];
    [self defineManager];
    return self;
}

- (void)defineDataFileManager {//init 1 kali aja
    self.datafileManager = [[OPTLYDatafileManagerDefault alloc] initWithBuilder:[OPTLYDatafileManagerBuilder builderWithBlock:^(OPTLYDatafileManagerBuilder * _Nullable builder) {
        builder.datafileConfig = [[OPTLYDatafileConfig alloc] initWithProjectId:nil withSDKKey:@"AE2w8bDJDXR148tunK9tnk"];
        builder.datafileFetchInterval = 120.0;
    }]];
}

- (void)defineManager {// init setiap kali dipangil utk refresh data baru
    self.manager = [[OPTLYManager alloc] initWithBuilder:[OPTLYManagerBuilder  builderWithBlock:^(OPTLYManagerBuilder * _Nullable builder) {
        builder.sdkKey = @"AE2w8bDJDXR148tunK9tnk";
        builder.datafileManager = self.datafileManager;
    }]];
    [self defineClient];
    [self setupNotifications];
    
}

- (void)defineClient {
    //using cached file
    self.client = [self.manager initialize];
    //async with callback
    [self.manager initializeWithCallback:^(NSError * _Nullable error,
                                      OPTLYClient * _Nullable client) {
        self.client = client;
    }];
}

- (void)setupNotifications{
    // Remove all notification listeners
    [[[self.manager getOptimizely] notificationCenter] clearAllNotificationListeners];
    // Add an Activate notification listener
    self.activateDecisionNotificationId = [[[self.manager getOptimizely] notificationCenter] addDecisionNotificationListener:^(NSString * _Nonnull type, NSString * _Nonnull userId, NSDictionary<NSString *,id> * _Nullable attributes, NSDictionary<NSString *,id> * _Nonnull decisionInfo) {
        NSLog(@"[!!!]");
    }];
}



- (void)getServices:(void (^)(Result * _Nonnull))completionBlock onError:(void (^)(NSError * _Nonnull))errorBlock {
    NSString *key = @"Services";
    
    BOOL enabled = [self.client isFeatureEnabled:key userId: @"bukalapak" attributes: nil];
    NSString *service_kitabisa = [self.client getFeatureVariableString:key variableKey: @"service_kitabisa" userId: @"bukalapak" attributes: nil];
    NSString *service_promo_claim = [self.client getFeatureVariableString:key variableKey: @"service_promo_claim" userId: @"bukalapak" attributes: nil];
    NSString *service_send_money_bank = [self.client getFeatureVariableString:key variableKey: @"service_send_money_bank" userId: @"bukalapak" attributes: nil];
    NSString *service_send_money_chat = [self.client getFeatureVariableString:key variableKey: @"service_send_money_chat" userId: @"bukalapak" attributes: nil];
    NSString *service_splitbill = [self.client getFeatureVariableString:key variableKey: @"service_splitbill" userId: @"bukalapak" attributes: nil];
    Result *result = [[[Result alloc] init]enabled:enabled service_kitabisa:service_kitabisa service_promo_claim:service_promo_claim service_send_money_bank:service_send_money_bank service_send_money_chat:service_send_money_chat service_splitbill:service_splitbill];
    completionBlock(result);
}

- (void)getSettings:(void (^)(Config * _Nonnull))completionBlock onError:(void (^)(NSError * _Nonnull))errorBlock {
    NSString *configKey = @"config";
        BOOL enabled = [self.client isFeatureEnabled:configKey userId: @"bukalapak" attributes: nil];
        
        NSString *settingsString = [self.client getFeatureVariableString:configKey variableKey: @"settings" userId: @"bukalapak" attributes: nil];
        NSArray *settingsStringArray = [self parseSettings:settingsString];
        
        Config *config = [[[Config alloc] init]settings:settingsStringArray];
        completionBlock(config);
}

- (NSArray*)parseSettings:(NSString*)input {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    NSMutableString *searchedString = [NSMutableString stringWithString:input];
    NSError* error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"([a-zA-Z_]+)" options:0 error:&error];
    NSArray* matches = [regex matchesInString:searchedString options:0 range:NSMakeRange(0, [searchedString length])];
    for ( NSTextCheckingResult* match in matches )
    {
        NSString* matchText = [searchedString substringWithRange:[match range]];
        [arr addObject:matchText];
    }
    return arr;
}

@end
