//
//  TestOptimizelyService.h
//  test_optimizely_framework
//
//  Created by Michael Gunawan on 10/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OptimizelySDKiOS/OptimizelySDKiOS.h>
#import "Result.h"
#import "Config.h"

NS_ASSUME_NONNULL_BEGIN

@interface TestOptimizelyService : NSObject
- (void)getServices:(nullable void (^)(Result *result))completionBlock onError:(nullable void (^)(NSError *error))errorBlock;
- (void)getSettings:(nullable void (^)(Config *config))completionBlock onError:(nullable void (^)(NSError *error))errorBlock;
@end

NS_ASSUME_NONNULL_END
