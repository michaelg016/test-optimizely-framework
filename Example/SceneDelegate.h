//
//  SceneDelegate.h
//  Example
//
//  Created by Michael Gunawan on 10/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

