//
//  AppDelegate.h
//  Example
//
//  Created by Michael Gunawan on 10/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

