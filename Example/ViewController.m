//
//  ViewController.m
//  Example
//
//  Created by Michael Gunawan on 10/03/20.
//  Copyright © 2020 Michael Gunawan. All rights reserved.
//

#import "ViewController.h"
#import "TestOptimizelyService.h"
//#import <OptimizelySDKiOS/OptimizelySDKiOS.h>
//#import <test_optimizely_framework/t>
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.textView.text = @"";
    //test from framework
    //[self parseSettings:@""];
    
    TestOptimizelyService *optimizelyService = [[TestOptimizelyService alloc] init];
    [optimizelyService getSettings:^(Config * _Nonnull config) {
        NSLog(@"[E]");
    } onError:^(NSError * _Nonnull error) {
        NSLog(@"[F]");
    }];
//
//    [optimizelyService getServices:^(Result * _Nonnull result) {
//        NSString *displayText = @"";
//        displayText = result.service_kitabisa;
//        displayText = [displayText stringByAppendingString:@"\n\n\n"];
//        displayText = [displayText stringByAppendingString:result.service_promo_claim];
//        displayText = [displayText stringByAppendingString:@"\n\n\n"];
//        displayText = [displayText stringByAppendingString:result.service_send_money_bank];
//        displayText = [displayText stringByAppendingString:@"\n\n\n"];
//        displayText = [displayText stringByAppendingString:result.service_send_money_chat];
//        displayText = [displayText stringByAppendingString:@"\n\n\n"];
//        displayText = [displayText stringByAppendingString:result.service_splitbill];
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//           self.textView.text = displayText;
//        });
//    } onError:^(NSError * _Nonnull error) {
//        NSLog(@"[!!!] Error: %@", error);
//    }];
}
//-(NSArray*)parseSettings:(NSString*)input{
//    NSArray *arr;
//    NSMutableString *searchedString = [NSMutableString stringWithString:@"[\"setting_tutorial\",\"setting_help\",\"setting_feedback\",\"setting_tnc\",\"setting_privacy_policy\"]"];
//    NSError* error = nil;
//
//    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"[a-zA-Z_]*" options:0 error:&error];
//    NSArray* matches = [regex matchesInString:searchedString options:0 range:NSMakeRange(0, [searchedString length])];
//    for ( NSTextCheckingResult* match in matches )
//    {
//        NSString* matchText = [searchedString substringWithRange:[match range]];
//        NSLog(@"match: %@", matchText);
//    }
//    return arr;
//}
//-(void)testOptimizelyLocal{
//    // Build a manager
//    OPTLYManager *manager = [[OPTLYManager alloc] initWithBuilder:[OPTLYManagerBuilder  builderWithBlock:^(OPTLYManagerBuilder * _Nullable builder) {
//        builder.sdkKey = @"RQcRisCUD1WCYjTZEdkbyv";
//    }]];
//
//    [manager initializeWithCallback:^(NSError * _Nullable error,
//                                      OPTLYClient * _Nullable client) {
//        BOOL enabled = [client isFeatureEnabled:@"test_two_instance_key" userId: @"userId" attributes: nil];
//        BOOL isOn = [[client getFeatureVariableBoolean:@"test_two_instance_key" variableKey: @"isOn" userId: @"userId" attributes: nil] boolValue];\
//        NSString *randomString = [client getFeatureVariableString:@"test_two_instance_key" variableKey: @"randomString" userId: @"userId" attributes: nil];
//
//        if(enabled){
//            NSLog(@"[!!!]test_two_instance_key is enabled");
//        }else{
//            NSLog(@"[!!!]test_two_instance_key is enabled");
//        }
//
//        if(isOn){
//            NSLog(@"[!!!]test_two_instance_key is isOn");
//        }else{
//            NSLog(@"[!!!]test_two_instance_key is isOff");
//        }
//        NSLog(@"[!!!]test_two_instance_key randomString: %@", randomString);
//    }];
//}


@end
